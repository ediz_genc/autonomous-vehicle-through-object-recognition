# 2022-group-06 (DIT638 Cyber Physical Systems and Systems of Systems)


<!--[![pipeline status](https://git.chalmers.se/courses/dit638/students/2022-group-06/badges/main/pipeline.svg)](https://git.chalmers.se/courses/dit638/students/2022-group-06/-/commits/main) -->

[![pipeline status](https://gitlab.com/ediz_genc/autonomous-vehicle-through-object-recognition/badges/main/pipeline.svg)](https://gitlab.com/ediz_genc/autonomous-vehicle-through-object-recognition/-/commits/main)

## What is the project?
Through microservices below software application performs object recognition frame by frame on the given recorded video file to create a self driving vehicle.  

- Opendlv-Vehicle-view
- Opendlv-h264-decoder
- Steering wheel system	

Each process handles the flowing data for a specific operation. Opendlv-vehicle-view gives the opportunity to display the footage from the vehicle camera and plot different data info. While these frames are broadcasted on the localhost by the previous process, Opendlv-h264-decoder gets access to these broadcasted frames and decodes them in a shared memory it creates. At this point the frames are ready to apply another process on them. The steering wheel system process is applying a filter to the frames to detect the cones contours in each frame so that we are able to calculate the steering wheel angle by calculating the angle between 2 consecutive contours in a row. 

There are also 2 actors in the following diagram. The vehicle demonstrates the camera device where the video frames come from and the Console, where the steering wheel angle calculation for each frame gets displayed.   

<img src="./assets/Decoding.png" width="651" height="338"><br>

## Why is the project created?
<img src="./assets/Contours.png" width="553" height="123"><br>
In order to create a full automated vehicle we needed to find the right values of the steering angle, the team has applied below steps:

- Isolate the region of interest from the given frames.
- Detect contours of blue cones using correct HSV values.
- Detect the center point of each contour.
- Find the point that represents the center of the car contour.
- Use three different points; center points of two neighboring cones and the point from the center of the car and lastly find the angle between them. 
## How has it been created?
This software solution for calculating the steering wheel angle for autonomous vehicles is based on the microservices and data-driven software design where each microservice executes a specific task on the flowing data between these microservices. 
In this software design there are 3 different microservices;

**OpenDLV vehicle view:** This microservice allows us to choose a recording file and display it along with its data on the localhost. In addition it also allows us to plot different data info and analyze it. Regarding our software solution, this microservice is used to provide the “OpenDLV h264 video decoder” the video frames.

**OpenDLV h264 video decoder:** The h264 decoder is used to decode frames into a shared memory that it creates from a broadcasted video. These frames shall be used later on to process and detect cones in order to calculate the steering wheel angle.

**Steering wheel angle system:** The steering wheel angle system gets access to the frames in the shared memory, it processes each frame and applies some operations to it so that it avoids noise as much as possible in the frames, marks a specific region of interest and detects the desired cones. When cones are detected then another operation is used to get the center points of these contours. As a last step the steering wheel angle formula gets these points as input to calculate the steering wheel angle. In the following 3.1 Software architecture diagram, there is a diagram to demonstrate how the software is designed.

<img src="./assets/microservices.png" width="580" height="397"><br>

## Algorithmic Aspects
A vehicle steering wheel angle is defined as the angle between the front of the vehicle and the steered wheel direction. The steering wheel angle can have both positive and negative values depending on the heading of the car. When a vehicle is heading north, the steering heading is 0 degrees. The vehicle has a positive heading in the counterclockwise direction and a negative heading in the clockwise direction. The steering wheel angle concept is visualized as below.

<img src="./assets/angle-concept.png" width="615" height="267"><br> 

<img src="./assets/blue-cones-detection.png" width="559" height="258"><br>

Discussing further how to find the x and y coordinates of the points in more detail. Points "A" and "B" are the centroids of the cone shapes that we have found. Point "O" is the top center of the car. To find the centroids of the cone shapes, our team decided to use OpenCV moments. We know that in computer vision each shape is made of pixels and the centroid of a shape is defined as the weighted average of all pixels constituting that shape. Image moment is the weighted average of image pixel intensities. Apart from helping to find image centroid, image moments can be used to find out other properties of a shape such as its area and radius.

# Getting started

## Run the project

1) Set up your Linux development environment as desired ,then run the following commands in the terminal;
```
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install build-essential cmake git
```
2) Install [Docker](https://docs.docker.com/engine/install/ubuntu/) to execute what is neccessary in virtual environment without additional installiton on your local machine.

3) Add your user to the group docker by following command to run Docker without superuser privileges using;
```
sudo usermod -aG docker $USER
```
_Note: You might need to log out from your session and log in again to be able to run the command docker without prepending sudo_

4) Install [Docker compose](https://docs.docker.com/compose/install)

To check that you have all the required tools, use the following commands:
```
g++ --version
make --version
cmake --version
git --version
docker --version
docker-compose --version
```
5) Create a new directory and clone the project repository with https key into that new directory if you would not like to create a SSH key.

_Note: To generate a public SSH key, follow [the SSH key documentation](https://docs.gitlab.com/ee/ssh/README.html#add-an-ssh-key-to-your-gitlab-account)_

6) Navigate to the directory where is located "dockerfile" on your system and type the following command to build the project repository by using docker container;

```
docker build -f Dockerfile -t <type-the-image-name> .
```
_Note: "type-the-image-name" replace with a name as you would like to have_


7) Download the examle [.rec file](https://git.chalmers.se/courses/dit638/students/2022-group-06/-/blob/main/assets/CID-140-recording-2020-03-18_145233-selection.rec)

8) Open a new terminal where you downloaded the example .rec file and type this command to run OpenDLV:
```
docker run --rm --init --net=host --name=opendlv-vehicle-view -v $PWD:/opt/vehicle-view/recordings -v /var/run/docker.sock:/var/run/docker.sock -p 8081:8081 chalmersrevere/opendlv-vehicle-view-multi:v0.0.60
```

9) Wait until OpenDLV-Vehicle-View is ready before you access via your web browser and then click on [the web browser (loacalhost:8081)](http://localhost:8081/)

10) To decompress the h264 frames we need the microservice h264 decoder. Run the following command in a new terminal to build the h264 decoder from its repository;
```
docker build https://github.com/chalmers-revere/opendlv-video-h264-decoder.git#v0.0.4 -f Dockerfile.amd64 -t h264decoder:v0.0.4
```

11) Open a new terminal window and type this command to be able to enable access to your graphical user interface (the X server);
```
xhost +
```

12) To inform the docker to allow access to the shared memory area between the h264decoder microservice and this project repository (2022-group-06), start the h264 decoder;
```
docker run --rm -ti --net=host --ipc=host -e DISPLAY=$DISPLAY -v /tmp:/tmp h264decoder:v0.0.4 --cid=253 --name=img
```
13) Now, we can start the the project repository (2022-group-06) as a microservice;
```
docker run --rm -ti --net=host --ipc=host -e DISPLAY=$DISPLAY -v /tmp:/tmp <type-the-image-name> --cid=253 --name=img --width=640 --height=480 --verbose 
```
_Note: Make sure the image name when you build the docker image same as above in the docker run command (i.e. "type-the-image-name" replace with the name you have created as you typed earlier when you build the docker image above)_

## Way of working
 ### Branching

Members shall avoid working in the master branch by creating a new branch named after the issue they are working on. Ex issue =  implement a login page -> branch name = “loginPage”. Use this command to create a new branch 
```
git checkout -b <desired branch name>
```
 In case the branch already exists then you can just map to this branch by typing this command in the terminal 
 
 ```
 git checkout <branch name>
 ```

 ### Committing

Before trying to push your code to the remote branch, members shall commit their changes first by  the following commands. Check what to commit by viewing the changes.

```
git status

```
then add specific files to commit by this command 
```
git add <filename>
```
or add all files by 

```
git add .
```

Now we can commit the changes and we do that by following our specific commit message template:

```
git commit -m "<i.e.following message template>" .
```

```
**Subject:**
_#(e.g. User story #2 or Bugfix x in class/method/function y)_

**Type:**
_#Problem (Main type (#bugFix, #userStory, ect))_

**Git Labels:**
_#Problem, #Task, #SprintX, #2 Reason for Commit Solution_

**Changes:**
_#Solution or List of Changes Note_ 

**Notes:**
_#Special instructions, testing steps, etc_

**Co-author:**
_#add the members as co-author "@"+ white-space between & followed by their gitlab account name (i.e. @<gitlab-account-name>)_
```

If the branch created is new then we need to push the changes by the following command:

```
git push --set-upstream origin <branch name>
```
Otherwise if the branch already exitsa and when the member has committed the changes to the branch, the remote branch shall be pulled and conflicts shall be resolved before pushing the new changes to this remote branch. To do this type this command in your terminal 

```
git pull
```
In case of conflicts there will be a message by the terminal which needs to be resolved. Follow and resolve the conflicts.

There will be a Discord channel in the team’s Discord server where team members shall announce and notify pushing to a specific remote branch to avoid pushing to the remote branch at the same time from different team members. 

When a green light is given to push to the remote branch, the team member can push to the remote branch by this command 

```
git push origin <branch name>

```
This also applies when trying to merge to master branch


### Merging to Main branch

- There will be no merging to the master branch unless an issue is completed or team members consider it necessary to merge to the master branch. To avoid conflicts, the team members are entitled to push and merge more often when it’s possible. 

- When an issue is done and needs to be merged to master, the team members shall create a pull request using the same commit template mentioned in point 4 with the appropriate tags and requesting reviews from other team members that have not worked on the entitled issue. If the pull request is approved and satisfies its criteria then it’s allowed to merge (the same template on point 4 will also be used here when merging to the master branch).

## Tools

- [G++](https://gcc.gnu.org/)
- [CMake](https://cmake.org/)
- [Docker](https://docs.docker.com/engine/)
- [Docker Compose](https://docs.docker.com/compose/)
- [Git](https://git-scm.com/)
- [C++](https://gcc.gnu.org/onlinedocs/libstdc++/)

## Testing

### White box testing
White box testing is a form of testing whereas a developer tests the inner workings of the software by firstly, evaluating and familiarizing themselves with the source code. The process for this usually starts off as manual analysis of the source code from the perspective of the tester. This is to allow the tester to understand the data flow of the software application and its inner structure. This traditionally falls into a process of testing called structural testing. When the tester finds their knowledge of the application sufficient, test cases can be drawn up or code that verifies a certain expected output of a function or algorithm as a whole. In our project the latter was applied the most whereas the tester usually ran all the environments for the software, such as the openldv-viewer and h264-decoder to properly mimic the environment the software has during runtime. 

### Algorithm/Data-flow testing
Data-flow testing, traditionally, is when a piece of software gets tested by varying the input the software receives. This could be a bit misleading in our case as this stage of testing never varied the inputs to our algorithm. We instead saw this stage as an algorithm break test instead. By supplying the software with test cases where we essentially run the whole algorithm from start to finish we could verify whether potential changes commited would break the software or not.

The algorithm tests were written using the Catch.hpp library and started off by sending an opencv Mat object(cv::Mat img) to the entry point of the algorithm and then declaring an expected value that would hopefully be outputted by the algorithm.

While we focused on performance during the white-box testing stage, the output we desired was steering angle and the quality of our test was determined by that as well. This type of testing had to be different and upon realizing this we came up with having the output be just an integer value of either 1 or 0 to represent true or false. True if the algorithm could run from start to end without breaking and false if it couldn't. By feeding the algorithm test images that we initialized into the opencv Mat object we could then sufficiently test our algorithm and compare the output to an expected value of 1, if equal it meant our algorithm did not break and our test passed.

This test runs every time you build the virtualization environment using docker and gets executed using cmake. The CMakeLists.txt had to be modified as well to add the executable test .cpp as well as the dependencies the test needed(OpenCV). In a way this also became our algorithm integration test.

<img src="./assets/algorithm-higher-perspective.png" width="678" height="509"><br>

## Developers

- [Bassam Eldesouki](https://git.chalmers.se/bassame)
- [Ediz Genc](https://git.chalmers.se/ediz)
- [Irina Levkovets](https://git.chalmers.se/irinale)
- [Meis Salim](https://git.chalmers.se/smeis)
- [Michael Araya](https://git.chalmers.se/arayam)

## License
[MIT license](https://git.chalmers.se/courses/dit638/students/2022-group-06/-/blob/main/LICENSE.md)
