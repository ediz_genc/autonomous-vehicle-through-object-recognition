#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

cv::Mat getRegionOfInterest(cv::Mat img)
{
  cv::Rect ROI = cv::Rect(0, 265, 600, 100);
  img = img(ROI);
  return img;
}
cv::Mat hsvConversion(cv::Mat img)
{
  cv::Mat hsvImage;
  // Convert image to hsv format
  cv::cvtColor(img, hsvImage, cv::COLOR_BGR2HSV);
  cv::Mat thresh;
  cv::inRange(hsvImage, cv::Scalar(92, 91, 25), cv::Scalar(155, 162, 255), thresh); // finding blue cones
  return thresh;
}
std::vector<std::vector<cv::Point>> detectBlueConeContours(cv::Mat img)
{
  cv::Mat hsvImage = hsvConversion(img);
  std::vector<std::vector<cv::Point>> contours;
  std::vector<cv::Vec4i> hierarchy;
  cv::findContours(hsvImage, contours, hierarchy, cv::RETR_TREE, cv::CHAIN_APPROX_NONE);
  cv::Mat image_copy = img.clone();
  cv::drawContours(image_copy, contours, -1, cv::Scalar(0, 255, 0), 2);
  return contours;
}
std::int16_t findConeCenter(cv::Mat img)
{
  std::vector<std::vector<cv::Point>> contours = detectBlueConeContours(img);
  cv::Mat image_copy = img.clone();
  std::vector<cv::Moments> mu(contours.size());
  std::vector<cv::Point2f> mc(contours.size());
  double steeringAngle;
  std::string angleString;
  for (size_t i = 0; i < contours.size(); i++)
  {
      mu[i] = cv::moments(contours[i]);
  }
  for (size_t i = 0; i < contours.size(); i++)
  {
      // add 1e-5 to avoid division by zero
      mc[i] = cv::Point2f(static_cast<float>(mu[i].m10 / (mu[i].m00 + 1e-5)),
                          static_cast<float>(mu[i].m01 / (mu[i].m00 + 1e-5)));
  }
  // for (size_t i = 0; i < contours.size(); i++)
  //{
    cv::drawContours(image_copy, contours, -1, cv::Scalar(0, 255, 0), 2);
      // Draw a circle to show the center point
    cv::circle(image_copy, mc[0], 4, cv::Scalar(0, 0, 255), -1);
 
    double numerator = (mc[1].y*(400 - mc[0].x)) + (400 * (mc[0].x - mc[1].x)) + (mc[0].y * (mc[1].y - 400));
  	double denominator = (mc[1].x - 400) * (400 - mc[0].x) + (mc[1].y - 400 - mc[0].x);
 
  	double ratio = numerator/denominator;
 
  	steeringAngle = static_cast<float>(atan(ratio));
	 
 	if (steeringAngle > 1 || steeringAngle < (-1)){
     	steeringAngle /= 100;
 	}else {
		 steeringAngle /= 16;
	}
    
 
   return 1;
}     


TEST_CASE("Test: Main Algorithm") {

    int16_t actual = 1;
    int16_t expected = 1;


    int test[11];
    std::string path;
    std::string image_path;
    int i = 0;

    for(i = 1; i <= 10; i++) {
        path = "/opt/sources/build/frame1.png";
      
        cv::Mat img = imread(path, cv::IMREAD_COLOR);
        if(img.empty()){
            throw std::invalid_argument("img is empty");
        }

        test[i] = findConeCenter(img);

    }
    for(i = 1; i < 11; i++) {
        if(!test[i]){
            actual = 0;
        }
    }

    REQUIRE(actual == expected);

}

