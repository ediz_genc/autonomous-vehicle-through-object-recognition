### How do we plan to collaborate?
* All the team members will follow an Agile methodology (SCRUM) to work on this project for which:
 
**SCRUM master**: Michael Araya

**Product owner**: Irina Levkovets

**Team members**: Ediz Genc, Meis Salim, Bassam Eldesouki 
	 
The Scrum master has the responsibility to oversee the team and its progress to ensure that deadlines are met and, in the case, where a member faces an obstacle, the scrum master shall, with the help of other team members, find a solution. 
 
### How do we ensure that everyone in our group stays informed about the individual contributions?
We will use Gitlab to create issues based on the user stories and assign team members to work on those issues.
We will monitor the contribution by closing the issues with commits.
 
### How do we ensure knowledge transfer among our team members?

There will be a channel for sharing notes and resources on our Discord server.
Individual team members can help each other by scheduling online meetings
 
### What is our usual communication plan?

Weekly meetings will be held three times a week, Mondays afternoon 13.15 - 15.15, Wednesdays at 13.00 - 13.15 and Fridays 13.00 - 15.00. Any changes must be discussed in advance and everyone has to agree on the change that is being discussed. Topics that are wished to be discussed during these meetings will be notified by team members to the SCRUM master in advance. 
 
### How will we solve conflicts?

* The responsibility to find solutions to overcome any obstacles lies on all team members.
* Team shall investigate the circumstances behind any non-performance to be able to address them and find solutions.
* In case of disrespectful or irresponsible behavior, this team member will be receiving a first warning. After a total of three warnings, the SCRUM master will inform Christian Berger, unless it is the Scrum Master that shows irresponsible behavior, then someone from the Development Team will inform Christian Berger.
* In case of a conflict where no resolution is close to being reached, there will have to be a vote. Given the number of members, the majority will have the final say.
